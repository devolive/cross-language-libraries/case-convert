import generalWords from "./general_words"

const DEFAULT_DELIMITER = " -_";

function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}

function buildRegex(str) {
    if (buildRegex.builded[str] === undefined) {
        let pattern = "";
        for (let i = str.length; --i >= 0;) {
            pattern += escapeRegExp(str[i]) + "|";
        }
        pattern = pattern.slice(0, -1);
        buildRegex.builded[str] = new RegExp(pattern);
    }
    return buildRegex.builded[str]
}
buildRegex.builded = {};

function getWords(input, options) {
    if (options.delimiter === undefined) {
        options.delimiter = DEFAULT_DELIMITER;
    }
    if (options.blacklist === undefined) {
        options.blacklist = [];
    }
    let result = input.split(buildRegex(options.delimiter));

    for (let i = result.length; --i >= 0;) {
        if (result[i] === "") {
            result.splice(i, 1);
        } else {
            for (const word of generalWords) {
                let wordSeparated = false;
                let startUpperCase = isUpperCase(result[i][0]);
                if (result[i].length > word.length + 2) {
                    let stillUpperCase = isUpperCase(result[i][word.length + 2]);
                    wordSeparated = startUpperCase !== stillUpperCase;
                }
                if (options.blacklist.indexOf(word) === -1 && result[i].toLowerCase().startsWith(word.toLowerCase()) && wordSeparated) {
                    result.splice(i, 1, ...[word, result[i].substring(word.length)]);
                }
            }
            let tmp = result[i].match(/[A-Z]*[a-z0-9]+(?=[A-Z]|$)|[A-Z]+(?=$)/g);
            if (tmp !== null && tmp.length > 1) {
                result.splice(i, 1, ...tmp);
            }
        }
    }
    return result;
}

export function camelCase(input, options = {}) {
    if (input === undefined || input === null) {
        return ""
    }
    const tmp = pascalCase(input, options);
    return tmp[0].toLowerCase() + tmp.substring(1);
}

export function kebabCase(input, options = {}) {
    if (input === undefined || input === null) {
        return "";
    }
    return getWords(input.toString(), options).join("-").toLowerCase();
}

export function pascalCase(input, options = {}) {
    if (input === undefined || input === null) {
        return ""
    }
    return getWords(input.toString(), options).map(w => w[0].toUpperCase() + w.toLowerCase().substring(1)).join("");
}

export function snakeCase(input, options = {}) {
    if (input === undefined || input === null) {
        return "";
    }
    return getWords(input.toString(), options).join("_").toLowerCase();
}

export function upperCase(input, options = {}) {
    if (input === undefined || input === null) {
        return "";
    }
    return getWords(input.toString(), options).join("_").toUpperCase();
}

export function isLowerCase(str) {
    return str === str.toLowerCase();
}

export function isUpperCase(str) {
    return str === str.toUpperCase();
}