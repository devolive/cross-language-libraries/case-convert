import { camelCase, kebabCase, pascalCase, snakeCase, upperCase } from "../src/index";
import camelTestCase from "../../unit_test_results/camel_case.csv"
import kebabTestCase from "../../unit_test_results/kebab_case.csv"
import pascalTestCase from "../../unit_test_results/pascal_case.csv"
import snakeTestCase from "../../unit_test_results/snake_case.csv"
import upperTestCase from "../../unit_test_results/upper_case.csv"

var undefinedVar;
let undefinedLet;

function generate_test(func, csvData) {
    let caseTested = 3;
    expect(func(undefinedVar)).toBe("");
    expect(func(undefinedLet)).toBe("");
    expect(func(null)).toBe("");
    for (let i = csvData.length; --i >= 0;) {
        //console.log(csvData[i]['argument']);
        expect(func(csvData[i]['argument'])).toBe(csvData[i]['result']);
        caseTested++;
    }
    console.log("Number of case tested with", func.name, ":", caseTested);
}

test('camelCase', () => {
    generate_test(camelCase, camelTestCase);
    expect(camelCase("APIErrors", {blacklist: ["API"]})).toBe("apierrors");
});

test('kebabCase', () => {
    generate_test(kebabCase, kebabTestCase);
});

test('pascalCase', () => {
    generate_test(pascalCase, pascalTestCase);
});

test('snakeCase', () => {
    generate_test(snakeCase, snakeTestCase);
});

test('upperCase', () => {
    generate_test(upperCase, upperTestCase);
});
