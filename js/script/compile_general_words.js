"use strict";

const fs = require("fs");
const path = require('path');

fs.readFile(path.join(__dirname, "../../general_words.txt"), 'utf8', (err, fileContent) => {
    let data = fileContent.split(/\n|\r|\r\n/);
    for (let i = data.length; --i >= 0;) {
        data[i] = data[i].trim().replace(/#.*/, "");
        if (data[i].length === 0) {
            data.splice(i, 1);
        }
    }
    fs.writeFile(path.join(__dirname, "../src/general_words.js"), "module.exports = " + JSON.stringify(data) + ";", err => {
        if (err) console.log("err", err);
    });
});
