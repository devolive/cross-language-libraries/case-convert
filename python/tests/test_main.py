import unittest
import csv
from typing import Optional

from case_convert import *


class CaseConvertTest(unittest.TestCase):

    def generate_test(self, function, file_name: Optional[str] = None):
        case_tested = 1
        self.assertEqual(function(None), "")
        if file_name is None:
            file_name = function.__name__ + ".csv"
        file_name = "../../unit_test_results/" + file_name
        with open(file_name, 'r') as csv_reader:
            reader = csv.DictReader(csv_reader)
            for row in reader:
                # print(function.__name__, row['argument'])
                self.assertEqual(row['result'], function(row['argument']))
                case_tested += 1
        print("Number of case tested with %s: %d" % (function.__name__, case_tested))

    def test_camel_case(self):
        self.generate_test(camel_case)
        self.assertEqual(camel_case("APIError", blacklist=["API"]), "apierror")

    def test_kebab_case(self):
        self.generate_test(kebab_case)

    def test_pascal_case(self):
        self.generate_test(pascal_case)

    def test_snake_case(self):
        self.generate_test(snake_case)

    def test_upper_case(self):
        self.generate_test(upper_case)
